import * as Hapi from "@hapi/hapi";
import { Request, ResponseToolkit, Server } from '@hapi/hapi';
import * as Path from 'path';
import * as DotEnv from 'dotenv';
import * as Inert from '@hapi/inert';
import * as Vision from '@hapi/vision';
import * as HapiSwagger from 'hapi-swagger';
import * as Boom from '@hapi/boom';

const Pack = require('../package.json');

export let server: Server;

declare module '@hapi/hapi' {
  interface RequestApplicationState {
    qb: any
  }
}

export const init = async function(): Promise<Server> {
  DotEnv.config({
    path: Path.join(process.env.PWD, '.env')
  });

  server = Hapi.server({
    port: process.env.API_PORT,
    routes: {
      cors: {
        origin: 'ignore',
      },
      validate: {
        failAction: async (request, h, err) => {
          if (process.env.DEBUG) {
            // During development, log and respond with the full error.
            console.error(err);
            throw err;
          } else {
            // In prod, log a limited error message and throw the default Bad Request error.
            console.error('ValidationError:', err.message);
            throw Boom.badRequest(`Invalid request payload input`);
          }
        },
      }
    }
  });

  // Swagger plugin registration
  const swaggerOptions = {
    info: {
      title: 'Test API Documentation',
      version: Pack.version,
    },
  };

  await server.register([
    Inert,
    Vision,
    {
      plugin: HapiSwagger,
      options: swaggerOptions
    }
  ]);

  await server.register({
    plugin: require('hapi-auto-route'),
    options: {
      routes_dir: Path.join(__dirname, 'routes'),
    }
  });

  // Add db connection
  server.ext('onPreAuth', async (request: Request, h: ResponseToolkit) => {

    request.app.qb = require('knex')({
      client: 'mysql',
      connection: {
        host : process.env.DB_HOST,
        port : process.env.DB_PORT,
        user : process.env.MYSQL_USER,
        password : process.env.MYSQL_PASSWORD,
        database : process.env.MYSQL_DATABASE
      }
    });

    return h.continue;
  });

  return server;
};

export const start = async function (): Promise<void> {
  console.log(`Listening on ${server.info.uri}`);
  return server.start();
};

process.on('unhandledRejection', (err) => {
  console.error("unhandledRejection");
  console.error(err);
  process.exit(1);
});
