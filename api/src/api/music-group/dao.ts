import { QueryResult } from '../../model/query.result';
import { helpers } from './helper';
import { MusicGroupResultInterface } from './result.interface';
import { MusicGroupDTO } from './dto';
import { Request } from '@hapi/hapi';

const MAX_PAGE_ITEMS = 100;
const TABLE_NAME = 'music_group';

export class MusicGroupDAO {
  findAll(params: QueryResult, request: Request): Promise<MusicGroupDTO> {
    const qb = request.app.qb({m: TABLE_NAME});

    if (params.search) {
      qb.where(b => {
        helpers.searchableFields.forEach(f => {
          b.orWhere(f, 'like', `%${params.search}%`);
        });
      });
    }

    return qb
      .select(helpers.selectableFields)
      .orderBy(params.sortBy || 'created_at', params.sortDir || 'DESC')
      .offset(params.offset)
      .limit(params.limit || MAX_PAGE_ITEMS)
      .then(rows => rows.map((r: MusicGroupResultInterface) => new MusicGroupDTO(r)));
  }

  findOneByUUID(uuid: string, request: Request): Promise<MusicGroupDTO|null> {
    const qb = request.app.qb({m: TABLE_NAME});

    return qb
      .select(helpers.selectableFields)
      .where('m.uuid', uuid)
      .first()
      .then((r: MusicGroupResultInterface) => r ? new MusicGroupDTO(r) : null);
  }

  count(request: Request, params?: QueryResult): Promise<number> {
    const qb = request.app.qb({m: TABLE_NAME});

    qb
      .count('id', {as: 'total'})
      .first();

    if (params && params.search) {
      qb.where(b => {
        helpers.searchableFields.forEach(f => {
          b.orWhere(f, 'like', `%${params.search}%`);
        });
      });
    }

    return qb.then(results => results['total']);
  }

  create(payload: {[key: string]: any}, request: Request) {
    const qb = request.app.qb(TABLE_NAME);

    return qb.insert(payload);
  }

  update(uuid: string, payload: {[key: string]: any}, request: Request) {
    const qb = request.app.qb(TABLE_NAME);

    return qb.update(payload).where('uuid', uuid);
  }

  delete(uuid: string, request: Request) {
    const qb = request.app.qb(TABLE_NAME);

    return qb.where('uuid', uuid).delete();
  }
}
