export class MusicGroupPayload {
  name: string
  origin: string
  city: string
  start: string
  end: string
  founders: string
  members_count: string
  musical_style: string
  description: string

  constructor(data: any) {
    this.name = data.name;
    this.origin = data.origin;
    this.city = data.city;
    this.start = data.start;
    this.end = data.end;
    this.founders = data.founders;
    this.members_count = data.members_count;
    this.musical_style = data.musical_style;
    this.description = data.description;
  }
}
