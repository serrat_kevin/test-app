import { MusicGroupResultInterface } from './result.interface';

export class MusicGroupDTO {
  uuid: string;
  name: string
  origin: string
  city: string
  start: string
  end: string
  founders: string;
  members: string
  musicalStyle: string
  description: string
  createdAt: Date

  constructor(data: MusicGroupResultInterface) {
    this.uuid = data.uuid;
    this.name = data.name;
    this.origin = data.origin;
    this.city = data.city;
    this.start = data.start;
    this.end = data.end;
    this.founders = data.founders;
    this.members = data.members;
    this.musicalStyle = data.musical_style;
    this.description = data.description;
    this.createdAt = data.created_at;
  }
}
