export const helpers = {
  selectableFields: [
    'm.id',
    'm.uuid',
    'm.name',
    'm.origin',
    'm.city',
    'm.start',
    'm.end',
    'm.founders',
    'm.members',
    'm.musical_style',
    'm.description',
    'm.created_at',
  ],
  searchableFields: [
    'm.name',
    'm.origin',
    'm.city',
    'm.founders',
    'm.musical_style',
    'm.description',
  ]
}
