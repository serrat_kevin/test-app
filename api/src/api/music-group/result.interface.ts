export interface MusicGroupResultInterface {
  id: number;
  uuid: string;
  name: string
  origin: string
  city: string
  start: string
  end: string
  founders: string;
  members: string
  musical_style: string
  description: string
  created_at: Date
}
