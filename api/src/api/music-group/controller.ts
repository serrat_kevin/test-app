import { Request, ResponseToolkit } from '@hapi/hapi';
import { MusicGroupDAO } from './dao';
import { QueryResult } from '../../model/query.result';
import * as Boom from '@hapi/boom';
import { MusicGroupPayload } from './payload';
import * as fs from 'fs';
import * as xlsx from 'xlsx';
import * as path from 'path';
import { v5 as uuidv5 } from 'uuid';

export class MusicGroupController {
  async get(request: Request, h: ResponseToolkit) {
    try {
      const queryResult = new QueryResult(request.query);
      const musicGroupDAO = new MusicGroupDAO();

      const [items, total, totalFiltered] = await Promise.all([
        musicGroupDAO.findAll(queryResult, request),
        musicGroupDAO.count(request, queryResult),
        musicGroupDAO.count(request),
      ]);

      return h.response({
        data: {
          items,
          total,
          totalFiltered,
          limit: queryResult.limit,
          offset: queryResult.offset || 0
        }
      });
    } catch (e) {
      console.error(e);
      throw Boom.badImplementation(e);
    }
  }

  async post(request: Request, h: ResponseToolkit) {
    try {
      const musicGroupDAO = new MusicGroupDAO();
      let payload = new MusicGroupPayload(request.payload);

      const res = await musicGroupDAO.create(payload, request);
      return h.response({
        data: res.shift()
      }).code(201);
    } catch (e) {
      console.error(e);
      throw Boom.badImplementation(e);
    }
  }

  async patch(request: Request, h: ResponseToolkit) {
    try {
      const musicGroupDAO = new MusicGroupDAO();
      const uuid = request.params.uuid;

      let payload = new MusicGroupPayload(request.payload);

      Object.keys(payload).forEach(key => payload[key] === undefined ? delete payload[key] : '');

      await musicGroupDAO.update(uuid, payload, request);

      return h.response({
        data: await musicGroupDAO.findOneByUUID(uuid, request)
      });
    } catch (e) {
      console.error(e);
      throw Boom.badImplementation(e);
    }
  }

  async delete(request: Request, h: ResponseToolkit) {
    try {
      const musicGroupDAO = new MusicGroupDAO();
      const uuid = request.params.uuid;

      await musicGroupDAO.delete(uuid, request);

      return h.response().code(204)
    } catch (e) {
      console.error(e);
      throw Boom.badImplementation(e);
    }
  }

  import = async (request: Request, h: ResponseToolkit) => {
    const { file } = request.payload as any;
    const extension = '.' + (file.hapi.filename.split('.').pop()).toLowerCase();

    // check file extension
    if (!['.xls', '.xlsx'].includes(extension)) {
      throw Boom.badRequest();
    }

    // clean filename
    // TODO stock uuid namespace into config file
    const uuid = uuidv5(new Date().getTime().toString() + file.hapi.filename, '6fe56234-d60d-5f5c-9b14-5dd9d46c06d0');
    const filename = uuid + extension;

    try {
      const uploadDir = path.join(__dirname, '../../resources/upload');

      // create file dir if no exists
      if (!fs.existsSync(uploadDir)) {
        fs.mkdirSync(uploadDir, { recursive: true });
      }

      // upload file
      const fullPath = [uploadDir, filename].join(path.sep);
      const fileHandle = fs.createWriteStream(fullPath);
      file.pipe(fileHandle);
      await new Promise((resolve, reject) => fileHandle.on('finish', resolve).on('error', reject));

      // import file data
      const spreadsheet = xlsx.readFile(fullPath);
      const excelData = xlsx.utils.sheet_to_json(spreadsheet.Sheets[spreadsheet.SheetNames[0]], {header: 1});
      const headers = excelData[0] as number[];

      // TODO improve headers file check
      if (9 !== headers.length) {
        throw Boom.internal('Bad headers');
      }

      const res = excelData.filter((f,i) => 0 !== i).map(r => {
        return {
          name: r[0] || null,
          origin: r[1] || null,
          city: r[2] || null,
          start: r[3] || null,
          end: r[4] || null,
          founders: r[5] || null,
          members: r[6] || null,
          musical_style: r[7] || null,
          description: r[8] || null,
        }
      });

      const musicGroupDAO = new MusicGroupDAO();
      await musicGroupDAO.create(res, request);

      return h.response().code(200);
    } catch(e) {
      throw Boom.badImplementation(e);
    }
  };
}
