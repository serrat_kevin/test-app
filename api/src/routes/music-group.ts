import { MusicGroupController } from '../api/music-group/controller';
import * as Joi from 'joi';

const musicGroupController = new MusicGroupController();

module.exports = [
  {
    method: 'GET',
    path: '/music-group',
    handler: musicGroupController.get,
    options: {
      validate: {
        query: Joi.object({
          search: Joi.string(),
          offset: Joi.number(),
          limit: Joi.number(),
          sortBy: Joi.string(),
          sortDir: Joi.string().valid('ASC', 'DESC', 'asc', 'desc'),
        })
      },
      tags: ['api']
    }
  },
  {
    method: 'POST',
    path: '/music-group',
    handler: musicGroupController.post,
    options: {
      validate: {
        payload: Joi.object().keys({
          name: Joi.string().not('').required(),
          origin: Joi.string().allow(null, ''),
          city: Joi.string().allow(null, ''),
          start: Joi.number().allow(null),
          end: Joi.number().allow(null),
          founders: Joi.string().allow(null, ''),
          members: Joi.number().allow(null),
          musicalStyle: Joi.string().allow(null, ''),
          description: Joi.string().allow(null, ''),
        })
      },
      tags: ['api'],
    }
  },
  {
    method: 'PATCH',
    path: '/music-group/{uuid}',
    handler: musicGroupController.patch,
    options: {
      validate: {
        params: Joi.object({
          uuid: Joi.string()
        }),
        payload: Joi.object().keys({
          name: Joi.string().not(''),
          origin: Joi.string().allow(null, ''),
          city: Joi.string().allow(null, ''),
          start: Joi.number().allow(null),
          end: Joi.number().allow(null),
          founders: Joi.string().allow(null, ''),
          members: Joi.number().allow(null),
          musicalStyle: Joi.string().allow(null, ''),
          description: Joi.string().allow(null, ''),
        })
      },
      tags: ['api'],
    }
  },
  {
    method: 'DELETE',
    path: '/music-group/{uuid}',
    handler: musicGroupController.delete,
    options: {
      validate: {
        params: Joi.object({
          uuid: Joi.string()
        })
      },
      tags: ['api'],
    }
  },
  {
    method: 'POST',
    path: '/music-group/import',
    handler: musicGroupController.import,
    options: {
      payload: {
        maxBytes: 3*1024*1024,
        multipart: {
          output: 'stream'
        },
      },
      validate: {
        payload: Joi.object({
          file: Joi.any().required().meta({ swaggerType: 'file' })
        })
      },
      plugins: {
        'hapi-swagger': {
          payloadType: 'form'
        }
      },
      tags: ['api']
    },
  }
];
