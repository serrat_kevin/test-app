import { Request, ResponseToolkit } from '@hapi/hapi';
import * as Boom from '@hapi/boom';

module.exports = [
  {
    method: 'GET',
    path: '/monitoring/test',
    options: {
      tags: ['api']
    },
    handler: async (request: Request, h: ResponseToolkit) => {
      try {
        return h.response('ok');
      } catch(e) {
        console.error(e);
        throw Boom.internal();
      }
    }
  }
];
