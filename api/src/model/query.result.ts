import { RequestQuery } from '@hapi/hapi';

export class QueryResult {
  offset: number;
  limit: number;
  sortBy: string;
  search?: string;
  sortDir?: string;

  constructor(data: RequestQuery) {
    this.offset = Number(data.offset);
    this.limit = Number(data.limit);
    this.sortBy = data.sortBy;
    this.search = data.search;
    this.sortDir = data.sortDir;
  }
}
