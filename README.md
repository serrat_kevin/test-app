# TEST AP

## Usage

This test project run under `Docker` (https://docs.docker.com/desktop/)

### Technologies used

- Front: `Angular 13.3`
- Api: `Node 14.15`
- Database: `MariaDB 10.7.3`

### Run development environment

**At the first time**, open a terminal and type this command: `make init`

**The other times** only `make start` command is necessary

When finished, go to [http://localhost:4200/](http://localhost:4200/) to see the project running.

If you want to see/manage the database with `phpMyAdmin`, go to [http://localhost:8081/](http://localhost:8081/). Credentials are `test` for both (not to be used in production of course).

Finally, you can access to the API route documentation here: [http://localhost:6969/documentation](http://localhost:6969/documentation)

### Create database table

At the project root, use `dump.sql` file to import it into `phpMyAdmin`.

### Shortcuts

A `Makefile` provides you many docker command shortcuts for this project.

For example, if you want to stop and remove all containers, lauch this command: `make clean`
