-- phpMyAdmin SQL Dump
-- version 5.1.3
-- https://www.phpmyadmin.net/
--
-- Hôte : db
-- Version du serveur : 10.7.3-MariaDB-1:10.7.3+maria~focal
-- Version de PHP : 8.0.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";

--
-- Base de données : `test-app`
--
CREATE DATABASE IF NOT EXISTS `test-app` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `test-app`;

-- --------------------------------------------------------

--
-- Structure de la table `music_group`
--

CREATE TABLE `music_group` (
                               `id` int(11) NOT NULL,
                               `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                               `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                               `origin` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                               `city` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                               `start` int(11) DEFAULT NULL,
                               `end` int(11) DEFAULT NULL,
                               `founders` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                               `members` int(11) DEFAULT NULL,
                               `musical_style` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                               `description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                               `created_at` datetime NOT NULL DEFAULT current_timestamp() COMMENT '(DC2Type:datetime_immutable)',
                               `updated_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déclencheurs `music_group`
--
DELIMITER $$
CREATE TRIGGER `generate_music_group_uuid` BEFORE INSERT ON `music_group` FOR EACH ROW SET NEW.uuid = UUID()
$$
DELIMITER ;

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `music_group`
--
ALTER TABLE `music_group`
    ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `music_group`
--
ALTER TABLE `music_group`
    MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;
