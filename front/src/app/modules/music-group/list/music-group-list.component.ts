import { Component, OnDestroy, OnInit } from '@angular/core';
import { finalize, Subscription, take } from 'rxjs';
import { QueryParams } from '../../../core/services/api.service';
import { MusicGroupService } from '../../../core/services/music-group.service';
import { LazyLoadEvent, MessageService } from 'primeng/api';

@Component({
  selector: 'music-group',
  templateUrl: './music-group-list.component.html',
  styleUrls: ['./music-group-list.component.scss']
})
export class MusicGroupListComponent implements OnInit, OnDestroy {

  subscriptions: Subscription[] = [];
  data: any[] = [];
  loading: boolean;
  rowsPerPage = 10;
  total: number;
  totalRecords: number;
  params: QueryParams;
  lazyLoadEvents: LazyLoadEvent;
  maxFileSize = 3 * 1024 * 1024;
  validFileExtensions = '.xls, .xlsx';
  cols: any[];

  constructor(protected musicGroupService: MusicGroupService,
              protected messageService: MessageService
  ) {}

  ngOnInit() {
    this.loading = false;
  }

  ngOnDestroy() {
    if (this.subscriptions.length > 0) {
      this.subscriptions.forEach((s) => s.unsubscribe());
    }
  }

  onLazyLoad($event: LazyLoadEvent) {
    this.loading = true;

    this.params = {
      offset: $event.first,
      limit: $event.rows,
      sortBy: $event.sortField,
      sortDir: $event.sortOrder,
      search: $event.globalFilter
    };

    this.lazyLoadEvents = $event;

    this.loadData();
  }

  loadData() {
    this.subscriptions.push(this.musicGroupService.get(this.params).pipe(
      take(1),
      finalize(() => {
        this.loading = false;
      }))
      .subscribe(results => {
        this.data = results.items;
        this.totalRecords = results.totalFiltered;
        this.total = results.total;
      })
    );
  }

  myUploader($event: any) {
    if ($event.files) {
      const formData = new FormData();
      const file = $event.files.shift();
      formData.append('file', file, file.name);
      this.subscriptions.push(this.musicGroupService.import(formData).subscribe(() => {
        this.loadData();

        this.messageService.add({severity:'success', summary:'Données importées', detail:'Vos données ont été importées avec succès'});
      }, () => {
        this.messageService.add({severity:'error', summary: 'Echec de l\'importation', detail:'Vos données n\'ont pas pu être importées. Veuillez réessayer.'});
      }));
    }
  }
}
