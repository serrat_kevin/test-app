import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MusicGroupListComponent } from './music-group-list.component';

describe('MusicGroupListComponent', () => {
  let component: MusicGroupListComponent;
  let fixture: ComponentFixture<MusicGroupListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MusicGroupListComponent ]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MusicGroupListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
