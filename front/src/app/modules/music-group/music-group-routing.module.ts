import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MusicGroupListComponent } from './list/music-group-list.component';

const routes: Routes = [
  {
    path: '',
    component: MusicGroupListComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MusicGroupRoutingModule {}
