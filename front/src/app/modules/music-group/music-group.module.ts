import { NgModule } from '@angular/core';
import { MusicGroupListComponent } from './list/music-group-list.component';
import { MusicGroupRoutingModule } from './music-group-routing.module';
import { CardModule } from 'primeng/card';
import { TableModule } from 'primeng/table';
import { CommonModule } from '@angular/common';
import { ToolbarModule } from 'primeng/toolbar';
import { FileUploadModule } from 'primeng/fileupload';

@NgModule({
  declarations: [
    MusicGroupListComponent
  ],
  imports: [
    MusicGroupRoutingModule,
    CardModule,
    TableModule,
    CommonModule,
    ToolbarModule,
    FileUploadModule
  ],
})
export class MusicGroupModule { }
