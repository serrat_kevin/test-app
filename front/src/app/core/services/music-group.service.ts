import { Injectable } from '@angular/core';
import { map, Observable } from 'rxjs';
import { ApiPayload, ApiService, QueryParams } from './api.service';
import { environment } from '../../../environments/environment';
import { HttpParams } from '@angular/common/http';

const baseUrl = environment.apiUrl + '/music-group';

@Injectable({
  providedIn: 'root',
})
export class MusicGroupService extends ApiService {
  get(queryParams: QueryParams): Observable<any> {
    let httpParams = new HttpParams()
      .set('offset', String((queryParams && queryParams.offset) ? queryParams.offset : 0))
      .set('limit', String((queryParams && queryParams.limit) ? queryParams.limit : 20))
      .set('sortBy', (queryParams && queryParams.sortBy) ? queryParams.sortBy : 'created_at')
      .set('sortDir', (queryParams && queryParams.sortDir === -1) ? 'ASC' : 'DESC')
    ;

    if (queryParams.search) {
      httpParams = httpParams.append('search', queryParams.search);
    }

    // TODO map with entity
    return this.http.get<ApiPayload>(`${baseUrl}`, {params: httpParams}).pipe(
      map(r => r.data)
    );
  }

  import(data: any) {
    return this.http.post<ApiPayload>(`${baseUrl}/import`, data);
  }

  //TODO add save (post and patch) + delete calls
}
