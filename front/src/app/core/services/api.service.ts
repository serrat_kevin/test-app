import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

export interface ApiPayload {
  data: any;
}

export interface QueryParams {
  limit?: number;
  offset?: number;
  search?: string;
  sortBy?: string;
  sortDir?: number;
}

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  constructor(protected http: HttpClient) {}
}
