#
# DOCKER COMMANDS
#
init: build start
build:
	docker-compose build
start:
	docker-compose up -d
clean:
	docker-compose down
bash-front:
	docker exec -e COLUMNS="`tput cols`" -e LINES="`tput lines`" -it test-app-front bash
bash-api:
	docker exec -e COLUMNS="`tput cols`" -e LINES="`tput lines`" -it test-app-api bash
logs-front:
	docker-compose logs --tail=100 -f front
logs-api:
	docker-compose logs --tail=100 -f api
